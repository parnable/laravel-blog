@extends('layouts.admin-master')

@section('styles')
    <link rel="stylesheet" href="{{ URL::to('/css/form.css') }}" type="text/css" />
@endsection

@section('content')
    <div class="container">
        @include('includes.info-box')
        <form action="" method="post">
            <div class="input-group">
                <label for="email">E-mail</label>
                <input type="text" name="email" {{ $errors->has('email') ? 'class=has-error' : ''}}
                value="{{ Request::old('email') }}">
            </div>
            <div class="input-group">
                <label for="password">Password</label>
                <input type="password" name="password" {{ $errors->has('password') ? 'class=has-error' : ''}}
                value="{{ Request::old('email') }}">
            </div>
            <button type="submit">Login</button>
            <input type="hidden" name="_token" value="{{ Session::token() }}">
        </form>
    </div>
    @endsection